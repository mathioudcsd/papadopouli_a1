% X= randn(100,1); % generate the signal X
% Y= sin([1:100]); % generate the signal Y
% [c,lags] = xcorr(X,Y,'coeff'); %compute the cross-correlation
% plot(lags,c) % plot the cross-correlation

fid = fopen('packet_header.txt');
x = textscan(fid, '%f %d %d %d %d %f %f %s %d %d %*[^\n]');
fclose(fid);

%%%%%%%%%%%%%

%dbstop if error


%%%%%%%%%%% 183 %%%%%%%%%%
k = find(x{3} == 183); %%k are the indices where 167 were found
for i = 1:size(k)  
    sums_bytes(:,i) = x{5}(k(i));
end

l = find(x{3} == 183); %%k are the indices where 167 were found
for i = 1:size(k)  
    durations(:,i) = x{6}(k(i));
end
figure();
X = sums_bytes; %sum of bytes of flows
X = double(X);
Y = durations;  %durations of flows
[c, lags] = xcorr(X,Y,'coeff');
plot(lags, c);
ylabel('corellation');
xlabel('lag');
title('xCorr for 183');
ylim([0 1])


%%%%%%%%%%% 167 %%%%%%%%%%

k = find(x{3} == 167); %%k are the indices where 167 were found
for i = 1:size(k)  
    sums_bytes(:,i) = x{5}(k(i));
end

l = find(x{3} == 167); %%k are the indices where 167 were found
for i = 1:size(k)  
    durations(:,i) = x{6}(k(i));
end
figure();
X = sums_bytes; %sum of bytes of flows
X = double(X);
Y = durations;  %durations of flows
[c, lags] = xcorr(X,Y,'coeff');
plot(lags, c);
ylabel('corellation');
xlabel('lag');
title('xCorr for 167');
ylim([0 1])

%%%%%%%%%%% 91 %%%%%%%%%%

k = find(x{3} == 91); %%k are the indices where 167 were found
for i = 1:size(k)  
    sums_bytes(:,i) = x{5}(k(i));
end

l = find(x{3} == 91); %%k are the indices where 167 were found
for i = 1:size(k)  
    durations(:,i) = x{6}(k(i));
end
figure();
X = sums_bytes; %sum of bytes of flows
X = double(X);
Y = durations;  %durations of flows
[c, lags] = xcorr(X,Y,'coeff');
plot(lags, c);
ylabel('corellation');
xlabel('lag');
title('xCorr for 91');
ylim([0 1])

%%%%%%%%%%% 143 %%%%%%%%%%

k = find(x{3} == 143); %%k are the indices where 167 were found
for i = 1:size(k)  
    sums_bytes(:,i) = x{5}(k(i));
end

l = find(x{3} == 143); %%k are the indices where 167 were found
for i = 1:size(k)  
    durations(:,i) = x{6}(k(i));
end
figure();
X = sums_bytes; %sum of bytes of flows
X = double(X);
Y = durations;  %durations of flows
[c, lags] = xcorr(X,Y,'coeff');
plot(lags, c);
ylabel('corellation');
xlabel('lag');
title('xCorr for 143');
ylim([0 1])

%%%%%%%%%%% 469 %%%%%%%%%%

k = find(x{3} == 469); %%k are the indices where 167 were found
for i = 1:size(k)  
    sums_bytes(:,i) = x{5}(k(i));
end

l = find(x{3} == 469); %%k are the indices where 167 were found
for i = 1:size(k)  
    durations(:,i) = x{6}(k(i));
end
figure();
X = sums_bytes; %sum of bytes of flows
X = double(X);
Y = durations;  %durations of flows
[c, lags] = xcorr(X,Y,'coeff');
plot(lags, c);
ylabel('corellation');
xlabel('lag');
title('xCorr for 469');
ylim([0 1])



