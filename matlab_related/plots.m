fid = fopen('arp');
arp = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);


% fid = fopen('classicstun');
% classicstun = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
% fclose(fid);


fid = fopen('browser');
browser = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

% fid = fopen('cattp');
% cattp = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
% fclose(fid);

fid = fopen('dns');
dns = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('http');
http = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('icmpv6');
icmpv6 = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

% fid = fopen('igmpv3');
% igmv3 = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
% fclose(fid);

fid = fopen('llmnr');
llmnr = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('nbns');
nbns = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

% fid = fopen('ntp');
% ntp = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
% fclose(fid);

fid = fopen('ssdp');
ssdp = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

% fid = fopen('stun');
% stun = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
% fclose(fid);

fid = fopen('tcp');
tcp = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('tlsv');
tlsv = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('udp');
udp= textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

%%% new %%%

fid = fopen('lldp');
lldp = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('mdns');
mdns = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('mp4');
mp4 = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('quic');
quic = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);

fid = fopen('tlsv1_2');
tlsv1_2 = textscan(fid, '%d %f %s %s %s %d %*[^\n]', 'headerlines',1);
fclose(fid);
%%%%%%%%%%%%%%  ex1_partb_i  %%%%%%%%%%%%%%%%%%%%%

arp_sum = sum(arp{6});
%classicstun_sum = sum(classicstun{6});
browser_sum = sum(browser{6});
%cattp_sum = sum(cattp{6});
dns_sum = sum(dns{6});
http_sum = sum(http{6});
icmpv6_sum = sum(icmpv6{6});
%igmv3_sum = sum(igmv3{6});
llmnr_sum = sum(llmnr{6});
nbns_sum = sum(nbns{6});
%ntp_sum = sum(ntp{6});
ssdp_sum = sum(ssdp{6});
%stun_sum = sum(stun{6});
tcp_sum = sum(tcp{6});
%tlsv_sum = sum(tlsv{6});
udp_sum = sum(udp{6});

lldp_sum = sum(lldp{6});
mdns_sum = sum(mdns{6});
mp4_sum = sum(mp4{6});
quic_sum = sum(quic{6});
tlsv1_2_sum=sum(tlsv1_2{6});


figure;
packet_size_sum_full = [http_sum, udp_sum, tcp_sum, arp_sum, dns_sum, icmpv6_sum, llmnr_sum, ssdp_sum, lldp_sum, mdns_sum, mp4_sum, quic_sum, tlsv1_2_sum, browser_sum, nbns_sum];
ar2_names = categorical({'http'; 'udp'; 'tcp'; 'arp'; 'dns'; 'icmpv6'; 'llmnr'; 'ssdp'; 'lldp'; 'mdns'; 'mp4'; 'quic'; 'tlsv1_2'; 'browser'; 'nbns' });
barh(ar2_names,packet_size_sum_full);
%set(gca, 'yticklabel', ar2_names);
ylabel('protocols');
xlabel('Size (bytes)');
title('Graph of the packet size sums (All protocols)');

figure;
packet_size_sum_smalls = [http_sum, udp_sum, arp_sum, dns_sum, icmpv6_sum, llmnr_sum, ssdp_sum, lldp_sum, mdns_sum, mp4_sum, browser_sum, nbns_sum];
ar22_names = categorical({'http'; 'udp'; 'arp'; 'dns'; 'icmpv6'; 'llmnr'; 'ssdp'; 'lldp'; 'mdns'; 'mp4'; 'browser'; 'nbns' });
barh(ar22_names,packet_size_sum_smalls);
%set(gca, 'yticklabel', ar2_names);
ylabel('protocols');
xlabel('Size (bytes)');
title('Graph of the packet size sums (All protocols)');

% figure(2)
% packet_size_sum_small = [arp_sum, classicstun_sum, browser_sum, cattp_sum, dns_sum, icmpv6_sum, igmv3_sum, llmnr_sum, nbns_sum, ntp_sum, ssdp_sum, stun_sum, tlsv_sum];
% ar2_names = {'arp'; 'classicstun'; 'browser'; 'cattp'; 'dns'; 'icmpv6'; 'igmv3'; 'llmnr'; 'nbns'; 'ntp'; 'ssdp'; 'stun'; 'tlsv' };
% barh(packet_size_sum_small);
% set(gca, 'yticklabel', ar2_names);
% ylabel('protocols');
% xlabel('Size (bytes)');
% title('Graph of the packet size sums (All protocols, except http, tcp, udp)');

% figure;
% std_plot = [arp_mean, classicstun_mean, browser_mean, cattp_mean, dns_mean, http_mean, icmpv6_mean, igmv3_mean, llmnr_mean, nbns_mean, ntp_mean, ssdp_mean, stun_mean, tcp_mean, tlsv_mean, udp_mean ];
% la = categorical( {'arp','classicstun','browser','cattp','dns','http','icmpv6','igmv3','llmnr','nbns','ntp','ssdp','stun','tcp','tlsv','udp'});
% barh(la,std_plot);
% ylabel('Protocols');
% xlabel('Packet Size');
% title('Mean values Graph of packet size for all protocols');

%%%%%%%%%%%%%%  ex1_partb_ii  %%%%%%%%%%%%%%%%%%%%%
figure;
ecdf(arp{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for arp protocol');

% figure;
% ecdf(classicstun{6});
% ylabel('probability');
% xlabel('Size (bytes)');
% title('cdf for classicstun protocol');

figure;
ecdf(browser{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for browser protocol');

% figure;
% ecdf(cattp{6});
% ylabel('probability');
% xlabel('Size (bytes)');
% title('cdf for cattp protocol');

figure;
ecdf(dns{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for dns protocol');

figure;
ecdf(http{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for http protocol');

figure;
ecdf(icmpv6{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for icmpv6 protocol');

% figure;
% ecdf(igmv3{6});
% ylabel('probability');
% xlabel('Size (bytes)');
% title('cdf for igmv3 protocol');

figure;
ecdf(llmnr{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for llmnr protocol');

figure;
ecdf(nbns{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for nbns protocol');

% figure;
% ecdf(ntp{6});
% ylabel('probability');
% xlabel('Size (bytes)');
% title('cdf for ntp protocol');

figure;
ecdf(ssdp{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for ssbp protocol');

% figure;
% ecdf(stun{6});
% ylabel('probability');
% xlabel('Size (bytes)');
% title('cdf for stun protocol');

figure;
ecdf(tcp{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for tcp protocol');

figure;
ecdf(tlsv1_2{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for tls protocol');

figure;
ecdf(udp{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for udp protocol');

%

figure;
ecdf(lldp{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for lldp protocol');

figure;
ecdf(mdns{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for mdns protocol');

figure;
ecdf(mp4{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for mp4 protocol');

figure;
ecdf(quic{6});
ylabel('probability');
xlabel('Size (bytes)');
title('cdf for quic protocol');

%%%%%%%%%%%%%%  ex1_partb_iii  %%%%%%%%%%%%%%%%%%%%%

arp_mean = mean(arp{6});
%classicstun_mean = mean(classicstun{6});
browser_mean = mean(browser{6});
%cattp_mean = mean(cattp{6});
dns_mean = mean(dns{6});
http_mean = mean(http{6});
icmpv6_mean = mean(icmpv6{6});
%igmv3_mean = mean(igmv3{6});
llmnr_mean = mean(llmnr{6});
nbns_mean = mean(nbns{6});
%ntp_mean = mean(ntp{6});
ssdp_mean = mean(ssdp{6});
%stun_mean = mean(stun{6});
tcp_mean = mean(tcp{6});
%tlsv_mean = mean(tlsv{6});
udp_mean = mean(udp{6});
lldp_mean = mean(lldp{6});
mdns_mean = mean(mdns{6});
mp4_mean = mean(mp4{6});
quic_mean = mean(quic{6});
tlsv1_2_mean=mean(tlsv1_2{6});



figure;

mean_plot = [http_mean, udp_mean, tcp_mean, arp_mean, dns_mean, icmpv6_mean, llmnr_mean, ssdp_mean, lldp_mean, mdns_mean, mp4_mean, quic_mean, tlsv1_2_mean, browser_mean, nbns_mean];
la = categorical({'http'; 'udp'; 'tcp'; 'arp'; 'dns'; 'icmpv6'; 'llmnr'; 'ssdp'; 'lldp'; 'mdns'; 'mp4'; 'quic'; 'tlsv1_2'; 'browser'; 'nbns' });
barh(la,mean_plot);
ylabel('Protocols');
xlabel('Packet Size');
title('Mean values Graph of packet size for all protocols');

% figure;
% std_plot = [arp_mean, classicstun_mean, browser_mean, cattp_mean, dns_mean, http_mean, icmpv6_mean, igmv3_mean, llmnr_mean, nbns_mean, ntp_mean, ssdp_mean, stun_mean, tcp_mean, tlsv_mean, udp_mean ];
% std_plot_names = {'arp','classicstun','browser','cattp','dns','http','icmpv6','igmv3','llmnr','nbns','ntp','ssdp','stun','tcp','tlsv','udp'};
% barh(std_plot);
% set(gca, 'yticklabel', std_plot_names);
% ylabel('Protocols');
% xlabel('Deviation');
% title('Graph of mean for all protocols');
%ylim([std_plot_names(1) std_plot_names(16)]);


arp_median = median(arp{6});
%classicstun_median = median(classicstun{6});
browser_median = median(browser{6});
%cattp_median = median(cattp{6});
dns_median = median(dns{6});
http_median = median(http{6});
icmpv6_median = median(icmpv6{6});
%igmv3_median = median(igmv3{6});
llmnr_median = median(llmnr{6});
nbns_median = median(nbns{6});
%ntp_median = median(ntp{6});
ssdp_median = median(ssdp{6});
%stun_median = median(stun{6});
tcp_median = median(tcp{6});
%tlsv_median = median(tlsv{6});
udp_median = median(udp{6});
lldp_median = median(lldp{6});
mdns_median = median(mdns{6});
mp4_median = median(mp4{6});
quic_median = median(quic{6});
tlsv1_2_median=median(tlsv1_2{6});

figure;
median_plot = [http_median, udp_median, tcp_median, arp_median, dns_median, icmpv6_median, llmnr_median, ssdp_median, lldp_median, mdns_median, mp4_median, quic_median, tlsv1_2_median, browser_median, nbns_median];
la = categorical({'http'; 'udp'; 'tcp'; 'arp'; 'dns'; 'icmpv6'; 'llmnr'; 'ssdp'; 'lldp'; 'mdns'; 'mp4'; 'quic'; 'tlsv1_2'; 'browser'; 'nbns' });
barh(la,median_plot);
ylabel('Protocols');
xlabel('Packet Size');
title('median values Graph of packet size for all protocols');

arp_std = std2(arp{6});
%classicstun_std = std(classicstun{6});
browser_std = std2(browser{6});
%cattp_std = std(cattp{6});
dns_std = std2(dns{6});
http_std = std2(http{6});
icmpv6_std = std2(icmpv6{6});
%igmv3_std = std(igmv3{6});
llmnr_std = std2(llmnr{6});
nbns_std = std2(nbns{6});
%ntp_std = std(ntp{6});
ssdp_std = std2(ssdp{6});
%stun_std = std(stun{6});
tcp_std = std2(tcp{6});
%tlsv_std = std(tlsv{6});
udp_std = std2(udp{6});
lldp_std = std2(lldp{6});
mdns_std = std2(mdns{6});
mp4_std = std2(mp4{6});
quic_std = std2(quic{6});
tlsv1_2_std=std2(tlsv1_2{6});

figure;
std_plot = [http_std, udp_std, tcp_std, arp_std, dns_std, icmpv6_std, llmnr_std, ssdp_std, lldp_std, mdns_std, mp4_std, quic_std, tlsv1_2_std, browser_std, nbns_std];
la = categorical({'http'; 'udp'; 'tcp'; 'arp'; 'dns'; 'icmpv6'; 'llmnr'; 'ssdp'; 'lldp'; 'mdns'; 'mp4'; 'quic'; 'tlsv1_2'; 'browser'; 'nbns' });
barh(la,std_plot);
ylabel('Protocols');
xlabel('Packet Size');
title('std values Graph of packet size for all protocols');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


