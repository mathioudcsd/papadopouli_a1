fid = fopen('packet_header.txt');
x = textscan(fid, '%f %d %d %d %d %f %f %s %d %d %*[^\n]');
fclose(fid);

%%%%%%%  183  %%%%%%%

k1 = find(x{3} == 183); %%k are the indices where 167 were found
for i = 1:size(k1)  
    sums_bytes183(:,i) = x{5}(k1(i));
end
mean183 = mean(sums_bytes183);
median183 = median(sums_bytes183);
std183 = std2(sums_bytes183);



k2 = find(x{3} == 167); %%k are the indices where 167 were found
for i = 1:size(k2)  
    sums_bytes167(:,i) = x{5}(k2(i));
end
mean167 = mean(sums_bytes167);
median167 = median(sums_bytes167);
std167 = std2(sums_bytes167);

k3 = find(x{3} == 91); %%k are the indices where 167 were found
for i = 1:size(k3)  
    sums_bytes91(:,i) = x{5}(k3(i));
end
mean91 = mean(sums_bytes91);
median91 = median(sums_bytes91);
std91 = std2(sums_bytes91);


k4 = find(x{3} == 143); %%k are the indices where 167 were found
for i = 1:size(k4)  
    sums_bytes143(:,i) = x{5}(k4(i));
end
mean143 = mean(sums_bytes143);
median143 = median(sums_bytes143);
std143 = std2(sums_bytes143);

k5 = find(x{3} == 469); %%k are the indices where 167 were found
for i = 1:size(k5)  
    sums_bytes469(:,i) = x{5}(k5(i));
end
mean469 = mean(sums_bytes469);
median469 = median(sums_bytes469);
std469 = std2(sums_bytes469);

figure;
mean_plot = [mean183, mean167, mean91, mean143, mean469];
mean_plot_names = {'m183', 'm167', 'm91', 'm143', 'm469'};
barh(mean_plot);
set(gca, 'yticklabel', mean_plot_names);
ylabel('APs');
xlabel('Mean');
title('Graph of mean flow size for all protocols');

figure;
median_plot = [median183, median167, median91, median143, median469];
median_plot_names = {'m183', 'm167', 'm91', 'm143', 'm469'};
barh(median_plot);
set(gca, 'yticklabel', median_plot_names);
ylabel('APs');
xlabel('median');
title('Graph of median flow size for all protocols');

figure;
std2_plot = [std183, std167, std91, std143, std469];
std2_plot_names = {'m183', 'm167', 'm91', 'm143', 'm469'};
barh(std2_plot);
set(gca, 'yticklabel', std2_plot_names);
ylabel('APs');
xlabel('std2');
title('Graph of std2 flow size for all protocols');
