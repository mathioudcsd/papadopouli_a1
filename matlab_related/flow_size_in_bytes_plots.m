fid = fopen('packet_header.txt');
x = textscan(fid, '%f %d %d %d %d %f %f %s %d %d %*[^\n]');
fclose(fid);

%%%%%%%%%%%%%

k = find(x{3} == 167); %%k are the indices where 167 were found
for i = 1:size(k)  
  cd(i,:) = x{5}(k(i));
end

figure;
ecdf(cd);
ylabel('probability');
xlabel('bytes');
title('CDF of the flow size in bytes AP 167 ');

%
k = find(x{3} == 183); 
for i = 1:size(k)  
  cd(i,:) = x{5}(k(i));
end

figure;
ecdf(cd);
ylabel('probability');
xlabel('bytes');
title('CDF of the flow size in bytes AP 183');

%
k = find(x{3} == 91); 
for i = 1:size(k)  
  cd(i,:) = x{5}(k(i));
end

figure;
ecdf(cd);
ylabel('probability');
xlabel('bytes');
title('CDF of the flow size in bytes AP 91');

%
k = find(x{3} == 143); 
for i = 1:size(k)  
  cd(i,:) = x{5}(k(i));
end

figure;
ecdf(cd);
ylabel('probability');
xlabel('bytes');
title('CDF of the flow size in bytes AP 143');

%
k = find(x{3} == 469); 
for i = 1:size(k)  
  cd(i,:) = x{5}(k(i));
end

figure;
ecdf(cd);
ylabel('probability');
xlabel('bytes');
title('CDF of the flow size in bytes AP 469');